//
//  ScoreField.swift
//  NYCSchools
//
//  Created by William Nkosi on 3/31/23.
//

import SwiftUI

struct ScoreField: View {
    let text: String
    let score: String
    var body: some View {
        HStack{
            Text(text)
                .font(.subheadline)
                .foregroundColor(.gray)
            Spacer()
            Text(score)
        }
    }
}


