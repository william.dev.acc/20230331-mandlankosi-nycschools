//
//  SchoolScoreView.swift
//  NYCSchools
//
//  Created by William Nkosi on 3/31/23.
//

import SwiftUI

struct SchoolScoreView: View {
    let school: School
    let listOfSchoolScores: [SchoolSATScore]
    @ObservedObject var viewModel =  SchoolScoreViewModel()
    var body: some View {
        ZStack{
            if viewModel.isLoading {
                ProgressView() .progressViewStyle(CircularProgressViewStyle(tint: Color.red))
            }
            if viewModel.isLoading == false {
                VStack(alignment: .leading, spacing: 8) {
                    Text(school.school_name ?? "Unknown")
                        .font(.headline)
                    ScoreField(text: "Average Critical Reading Score:", score: viewModel.schoolResults?.sat_critical_reading_avg_score ?? "N/A")
                    ScoreField(text: "Average Math Score:", score: viewModel.schoolResults?.sat_math_avg_score ?? "N/A")
                    ScoreField(text: "Average Writing Score:", score: viewModel.schoolResults?.sat_writing_avg_score ?? "N/A")
                }
                .padding()
                .background(Color.white)
            }
            
            
        }.onAppear{
            viewModel.findSchoolResult(schoolID:school.dbn ?? "unknown name" , listOfSchoolScores: listOfSchoolScores)
        }
        
        
    }
    
}




