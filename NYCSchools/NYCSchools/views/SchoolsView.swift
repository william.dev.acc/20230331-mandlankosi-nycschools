//
//  SchoolsView.swift
//  NYCSchools
//
//  Created by William Nkosi on 3/31/23.
//

import SwiftUI

struct SchoolView: View {
    @ObservedObject var viewModel = SchoolsViewModel()
    
    var body: some View {
      
        NavigationView {
            ZStack{
                if viewModel.isLoading {
                    ProgressView() .progressViewStyle(CircularProgressViewStyle(tint: Color.red))
                }
                
                if viewModel.isLoading == false {
                VStack{
                   
                    List(viewModel.schools,id: \.self) {schools in
                        NavigationLink(destination: SchoolScoreView(school: schools, listOfSchoolScores: viewModel.schoolsScores)){ Button(action: {
                            
                        }) {
                            Text(schools.school_name ?? "N/A School name not found." )
                        }
                        }
                        
                    }
                }
                }
              
            }
            .navigationTitle("NYC Schools")
            .onAppear {
                viewModel.fetchSchools()
                
            }
        }
        
    }
}

