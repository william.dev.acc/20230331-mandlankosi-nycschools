//
//  NycSchoolsService.swift
//  NYCSchools
//
//  Created by William Nkosi on 3/31/23.
//

import Foundation

class NycSchoolService {
    func fetchSchools(completion: @escaping ([School]?, Error?) -> Void)  {
        
        let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
               
                return
            }
            
            do {
                let schools = try JSONDecoder().decode([School].self, from:data)
                completion( schools, nil)
                
            } catch {
               
                completion(nil, error)
            }
        }.resume()
        
    }
    
    func fetchSchoolsScores(completion: @escaping ([SchoolSATScore]?, Error?) -> Void) {
        let url = URL(string:"https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
           
                return
            }
            
            do {
                let schoolsScores = try JSONDecoder().decode([SchoolSATScore].self, from:data)
                completion( schoolsScores , nil)
            } catch {
                completion(nil, error)
            }
        }.resume()
    }
    
    func findSchoolScores(schoolID:String, schoolSATScores: [SchoolSATScore]) -> SchoolSATScore? {
        if let school = schoolSATScores.first(where: {$0.dbn == schoolID}) {
            return school
        } else {
            return nil
        }
    }
    
}
