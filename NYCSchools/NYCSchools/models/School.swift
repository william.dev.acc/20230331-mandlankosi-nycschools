//
//  School.swift
//  NYCSchools
//
//  Created by William Nkosi on 3/31/23.
//

import Foundation

struct School: Decodable, Hashable {
    var dbn:String?
    var school_name: String?
}
