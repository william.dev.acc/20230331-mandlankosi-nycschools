//
//  SchoolSATScore.swift
//  NYCSchools
//
//  Created by William Nkosi on 3/31/23.
//

import Foundation

struct SchoolSATScore:Hashable, Decodable  {
    var dbn:String?
    var sat_critical_reading_avg_score:String?
    var sat_math_avg_score: String?
    var sat_writing_avg_score:String?
}
