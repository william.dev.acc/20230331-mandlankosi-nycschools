//
//  SchoolsViewModel.swift
//  NYCSchools
//
//  Created by William Nkosi on 3/31/23.
//

import Foundation
import SwiftUI

class SchoolsViewModel: ObservableObject {
    @Published var schools : [School] = []
    @Published var schoolsScores : [SchoolSATScore] = []
    @Published var isLoading = false
    let nycSchoolService = NycSchoolService()
    
    func fetchSchools(){
        isLoading = true
        nycSchoolService.fetchSchools{ [weak self] schools, error in
            if let error = error {
                print("Error fetching people: \(error)")
            } else if let schools = schools {
                DispatchQueue.main.async {
                    self?.isLoading = false
                    self?.schools = schools
                    
                }
            }
        }
        
        self.fetchSchoolScore()
    }
    
    func fetchSchoolScore() {
        nycSchoolService.fetchSchoolsScores { [weak self] schoolsScores, error in
            if let error = error {
                print("Error fetching people: \(error)")
            } else if let schoolsScores = schoolsScores {
                DispatchQueue.main.async {
                    self?.schoolsScores = schoolsScores
                }
            }
        }
    }
    
    
}
