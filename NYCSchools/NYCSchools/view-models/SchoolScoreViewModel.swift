//
//  SchoolScoreViewModel.swift
//  NYCSchools
//
//  Created by William Nkosi on 3/31/23.
//

import Foundation

class SchoolScoreViewModel: ObservableObject {
    @Published var schoolResults: SchoolSATScore?;
    @Published var isLoading = false
    let nycSchoolService = NycSchoolService()
    
    func findSchoolResult(schoolID:String, listOfSchoolScores: [SchoolSATScore]){
        isLoading = true
    
        let results =  nycSchoolService.findSchoolScores(schoolID: schoolID, schoolSATScores: listOfSchoolScores) ?? nil
        DispatchQueue.main.async {
            self.isLoading = false
            self.schoolResults = results
            
        }
    }
}
