//
//  NYCSchoolsApp.swift
//  NYCSchools
//
//  Created by William Nkosi on 3/31/23.
//

import SwiftUI

@main
struct NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
           SchoolView()
        }
    }
}
